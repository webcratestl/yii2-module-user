# Install
You must have an authManager instance set in your application.  The permissions available for the module are as follows:
```
create-user
update-user
view-user
delete-user
```
composer.json file
```json
    "repositories": [
		{
			"type": "vcs",
			"url": "https://bitbucket.org/stephencozart/yii-module-user"
		}
	],
	"required": [
	    ...
	    "stephencozart/yii-module-user":"dev-master"
	]
```
Composer
```sh
composer install
```
Migrations
```sh
yii migrate  --migrationPath=@vendor/stephencozart/yii-module-user/src/migrations
```
You can use the User model of the module as the identity for your application otherwise you can role your own.  If you
want to do the former just set the identity of the user component:
```php
'user'=>[
    ...
    'identity'=>'stephencozart\yii2\modules\user\models\User'
]
```
There are certain events you can listen to.  This will allow you to send emails to a user after they have registered for example.
```
sc.user.register //fired after a user has registered
sc.user.forgot-password //fired after the user submits the forgot password form
sc.user.forgot-password-complete //fired after the user sets their new password
```
Example on how to use the events:
```
Yii::$app->on('sc.user.register', function(\yii\base\Event $event) {
	/** @var stephencozart\yii2\modules\user\models\User $model */
	$model = $event->sender;

    $scheme = Yii::$app->request->isSecureConnection ? 'https':true;
    $emailData = [
        'name'=>$model->first_name ? $model->first_name : 'there',
        'signature'=>isset(Yii::$app->params['emailSignature']) ? Yii::$app->params['emailSignature'] : $_SERVER['HTTP_HOST'] . ' team',
        'activationUrl'=>\yii\helpers\Url::to(['activate','code'=>$model->activation_code], $scheme)
    ];

    $from = isset(Yii::$app->params['emailFrom']) ? Yii::$app->params['emailFrom'] : sprintf('noreply@%s', $_SERVER['HTTP_HOST']);

    $message = Yii::$app->mailer->compose()
        ->setFrom($from)
        ->setTo($model->email)
        ->setSubject('Account Confirmation')
        ->setHtmlBody('<h1>Hi!!!!</h1>');

    $message->send();

});
```