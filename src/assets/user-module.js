(function($) {

	$('[data-toggle=\"multiselect\"]').scMultiSelect();

	var userModuleModal = $('#user-module-modal');
	userModuleModal.on('show.bs.modal', function (event) {
		var modal = $(this);
		var button = $(event.relatedTarget);


		modal.find('.modal-title').text(button.data('title'));
		$.get(button.data('url'), function(r) {
			modal.find('#modal-content-target').html(r);
			$('[data-toggle=\"multiselect\"]', modal).scMultiSelect({
				controlPadding: '30px'
			});
		});
	});

	userModuleModal.on('hidden.bs.modal', function(event) {
		var rolesGridView = $('#roles-grid-view');

		if (rolesGridView.length > 0) {
			var data = rolesGridView.yiiGridView('data');
			var url = data.settings.filterUrl;
			$.get(url, function(r) {
				var html = $(r).find('#roles-grid-view').html();
				rolesGridView.html(html);
			});
		}
	});

	userModuleModal.on('click', '[data-type="submit"]', function() {
		var _this = $(this);
		var form = _this.closest('.modal').find('form');
		form.trigger('submit');
	});

	$(document).on('pjax:complete', function(xhr, options) {

		userModuleModal.find('[data-toggle=\"multiselect\"]').scMultiSelect({
			controlPadding: '30px'
		});
	});
})(jQuery);