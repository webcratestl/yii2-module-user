(function ($) {
	$.fn.scMultiSelect = function (method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.yiiGridView');
			return false;
		}
	};

	var defaults = {
		controlPadding: null
	};

	var methods = {
		init: function (options) {
			return this.each(function () {
				var _this = $(this);
				var settings = $.extend({}, defaults, options || {});

				var target = $(_this.data('target'));
				var controlsTarget = $(_this.data('controls'));

				var label = $('<label />');
				label.text(_this.data('label'));
				target.html('');
				target.append(label);

				var select = $('<select multiple=\"multiple\" class=\"form-control\" />')
				select.attr('name', _this.data('name'));
				target.append(select);

				var controlsCss = {
					display: 'block',
					width: '100%',
					textAlign: 'center',
					width: '25px',
					height: '25px',
					background: '#eee',
					marginBottom: '3px',
					marginLeft: 'auto',
					marginRight: 'auto',
					cursor: 'pointer'
				};

				var addControl = $('<span />');
				addControl.css(controlsCss).html('&rarr;');
				addControl.on('click', function() {
					var selected = $(':selected', _this);
					selected.each(function() {
						select.append($(this));
					});
					_this.find('option:eq(0)').attr('selected','selected');
				});

				var removeControl = $('<span />');
				removeControl.css(controlsCss).html('&larr;');
				removeControl.on('click', function() {
					var selected = $(':selected', select);
					selected.each(function() {
						_this.append($(this));
					});
				});

				controlsTarget.html('');
				controlsTarget.append(addControl);
				controlsTarget.append(removeControl);

				var padding = settings.controlPadding;
				if (settings.controlPadding === null) {
					var diff = ($(_this.data('controls')).height()/2) + $(_this.data('controls')).height();
					padding = $(_this.data('target')).height() - diff;
				} else {

				}

				controlsTarget.css('padding-top', padding);

				$(':selected', _this).each(function() {
					select.append($(this));
				});
			});
		}
	};

})(jQuery);