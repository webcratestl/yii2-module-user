<?php
namespace stephencozart\yii2\modules\user;

use Yii;
use yii\base\InvalidConfigException;

class Module extends \yii\base\Module {

	public function init()
	{
		parent::init();
		self::initLocale();
    }

	public static function t($message, $params = [], $language = null)
	{
		return \Yii::t('StephenCozart/Modules/User', $message, $params, $language);
	}

	public static function getSystemRoles()
	{
		if (Yii::$app->authManager) {
			return Yii::$app->authManager->getRoles();
		}
		return [];
	}

	public static function initLocale()
	{
		\Yii::$app->i18n->translations['StephenCozart/Modules/User'] = [
			'class' => 'yii\i18n\PhpMessageSource',
			'sourceLanguage' => 'en-US',
			'basePath' => __DIR__ . '/messages',
			'fileMap'=>[
				'StephenCozart/Modules/User'=>'default.php'
			]
		];
	}

	public static function generateRandomString()
	{
		try {
			$token = \Yii::$app->getSecurity()->generateRandomString();
		} catch (InvalidConfigException $e) {
			$token = md5(uniqid(rand(), true));
		}
		return $token;
	}
}
