<?php
/**
 * Created by JetBrains PhpStorm.
 * User: stephencozart
 * Date: 7/26/15
 * Time: 3:25 PM
 * To change this template use File | Settings | File Templates.
 */

namespace stephencozart\yii2\modules\user\models;

use stephencozart\yii2\modules\user\Module;
use yii\base\Model;
use Yii;

class Role extends Model {

	public $name;
	public $description;
	public $permissions = [];

	public function rules()
	{
		return [
			[['name','description'],'required'],
			[['name'], 'validateName', 'on'=>'insert'],
			[['permissions'], 'each', 'rule'=>['string']]
		];
	}

	public function validateName()
	{
		if ($authManager = Yii::$app->authManager) {
			if ($role = $authManager->getRole($this->name)) {
				$this->addError('name', '{role} already exists');
			}

		}

	}

	public function save($validate=true)
	{
		if ($validate === false || $this->validate()) {
			$transaction = Yii::$app->db->beginTransaction();

			try {
				if (($authManager = Yii::$app->authManager) === null) {
					throw new \Exception('Auth Manager is not enabled');
				}

				$role = $authManager->getRole($this->name);
				if ($role === null) {
					$role = $authManager->createRole($this->name);
					$role->description = $this->description;
					$authManager->add($role);
				}

				$authManager->removeChildren($role);

				foreach($this->permissions as $permissionName) {
					if ($permission = $authManager->getPermission($permissionName)) {
						$authManager->addChild($role, $permission);
					}
				}

				$transaction->commit();
				return true;
			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			}

		}
		return false;
	}

	public function getAttributeLabel($attribute)
	{
		return Module::t(parent::getAttributeLabel($attribute));
	}
}
