<?php

namespace stephencozart\yii2\modules\user\models;

use stephencozart\yii2\modules\user\Module;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $username
 * @property string $password
 * @property string $activation_code
 * @property string $reset_code
 * @property string $auth_key
 * @property integer $status_id
 * @property string $created_on
 * @property string $updated_on
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 99;
	const STATUS_BANNED = 100;

	const SCENARIO_REGISTER = 'register';
	const SCENARIO_LOGIN = 'login';
	const SCENARIO_CREATE_CONSOLE = 'console-create';
	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_ACTIVATE = 'activate';

	public $roleNames = [];

	public $password_compare;

	public function afterFind()
	{
		if ($authManager = Yii::$app->authManager) {
			foreach(Yii::$app->authManager->getRolesByUser($this->id) as $role) {
				$this->roleNames[] = $role->name;
			}
		}

		parent::afterFind();
	}

	public function getStatusName()
	{
		$list = self::getStatusList();
		return isset($list[$this->status_id]) ? $list[$this->status_id] : '(not set)';
	}

	public static function getStatusList()
	{
		return [
			self::STATUS_ACTIVE=>Yii::t('StephenCozart/Modules/User','Active'),
			self::STATUS_INACTIVE=>Yii::t('StephenCozart/Modules/User','In-Active'),
			self::STATUS_BANNED=>Yii::t('StephenCozart/Modules/User','Banned'),
		];
	}

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

	        [['username','password', 'email'], 'required', 'on'=>self::SCENARIO_CREATE_CONSOLE],
	        [['username','password','email','first_name','last_name'], 'required','on'=>self::SCENARIO_REGISTER],

	        [['password_compare'], 'compare', 'compareAttribute'=>'password', 'on'=>self::SCENARIO_ACTIVATE],
	        [['email'], 'email'],
	        [['username'], 'unique','on'=>[self::SCENARIO_CREATE_CONSOLE, self::SCENARIO_REGISTER, self::SCENARIO_CREATE]],
	        [['email'], 'unique','on'=>[self::SCENARIO_CREATE_CONSOLE, self::SCENARIO_REGISTER, self::SCENARIO_CREATE]],
            [['status_id'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['first_name', 'last_name', 'email', 'username', 'password', 'activation_code', 'reset_code', 'auth_key'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'username' => 'Username',
            'password' => 'Password',
            'activation_code' => 'Activation Code',
            'reset_code' => 'Reset Code',
            'status_id' => 'Status',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function getAuthKey()
	{
		return $this->auth_key;
	}

	/**
	 * Finds user by username
	 *
	 * @param  string      $username
	 * @return static|null
	 */
	public static function findByUsername($username)
	{
		return self::find()->where(['username'=>$username])->one();
	}

	/**
	 * Finds user by email
	 *
	 * @param  string      $email
	 * @return static|null
	 */
	public static function findByEmail($email)
	{
		return self::find()->where(['email'=>$email])->one();
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		return self::find()->where(['auth_key'=>$token])->one();
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentity($id)
	{
		$User = self::findOne($id);
		return $User ? $User : null;
	}

	/**
	 * @inheritdoc
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @inheritdoc
	 */
	public function validateAuthKey($authKey)
	{
		return $this->auth_key === $authKey;
	}

	public function behaviors()
	{
		return [
			[
				'class' => TimestampBehavior::className(),
				'value' => new Expression('NOW()'),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_on', 'updated_on'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_on'],
				],
			],
		];
	}

	public function getAttributeLabel($attribute)
	{
		return Module::t(parent::getAttributeLabel($attribute));
	}
}
