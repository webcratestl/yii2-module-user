<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use stephencozart\yii2\modules\user\models\User;
use stephencozart\yii2\modules\user\Module;


/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model User */

?>

<div class="user-register-wrapper">
	<div class="user-register-header">
		<h1><?= Module::t('Register') ?></h1>
	</div>
	<div class="user-register-body">
		<?php $form = ActiveForm::begin([
			'id' => 'login-form',
			'options' => ['class' => 'form-horizontal'],
			'fieldConfig' => [
				'template' => "<div class='col-lg-12'>{label}\n{input}\n{error}</div>",
				'labelOptions' => ['class' => 'control-label'],
			],
		]); ?>

		<?= $form->field($model, 'first_name') ?>

		<?= $form->field($model, 'last_name') ?>

		<?= $form->field($model, 'username') ?>

		<?= $form->field($model, 'email') ?>

		<?= $form->field($model, 'password')->passwordInput() ?>

		<div class="form-group text-right">
			<hr>
			<?= Html::submitButton(Module::t('Register'), ['class' => 'btn btn-primary', 'name' => 'register-button']) ?>
		</div>

		<?php ActiveForm::end() ?>
	</div>
</div>