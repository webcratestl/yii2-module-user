<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use stephencozart\yii2\modules\user\models\User;
use stephencozart\yii2\modules\user\Module;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model User */

$this->title = Module::t('Reset Password');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-activate-wrapper">

	<div class="login-widget animation-delay1">
		<div class="panel panel-default">
			<div class="panel-heading clearfix">
				<div class="pull-left">
					<i class="fa fa-lock fa-lg"></i> <?= $this->title; ?>
				</div>
			</div>
			<div class="panel-body">

				<p><?= Module::t('Please fill out the following fields to reset your password.') ?></p>

				<?php $form = ActiveForm::begin([
					'id' => 'login-form',
					'options' => ['class' => 'form-horizontal'],
					'fieldConfig' => [
						'template' => "<div class='col-lg-12'>{label}\n{input}\n{error}</div>",
						'labelOptions' => ['class' => 'control-label'],
					],
				]); ?>

				<?= $form->field($model, 'email') ?>

				<?= $form->field($model, 'password')->passwordInput() ?>

				<?= $form->field($model, 'password_compare')->passwordInput() ?>

				<div class="seperator"></div>

				<hr>
				<?= Html::submitButton(Module::t('Reset Password'), ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>


				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>

</div>
