<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use stephencozart\yii2\modules\user\Module;

/* @var $this yii\web\View */
/* @var $model stephencozart\yii2\modules\user\models\Role */
/* @var $form yii\widgets\ActiveForm */

?>
<div class="container-fluid">

	<?php $form = ActiveForm::begin([
		'id'=>'role-form',
		'options'=>[
			'data-pjax'=>''
		],
		'fieldConfig' => [
			'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
		],
	]); ?>

	<div class="row">
		<div class="col-md-5">
			<?= $form->field($model, 'name') ?>

			<?= $form->field($model, 'description')->textarea() ?>
		</div>
		<div class="col-md-3">
			<label for="availableRoles"><?= Module::t('Available') ?></label>
			<?= Html::dropDownList('permissions', $model->permissions, $permissionsListData, [
				'multiple'=>'multiple',
				'class'=>'form-control',
				'data-toggle'=>'multiselect',
				'data-target'=>'#select-permissions',
				'data-controls'=>'#select-permission-controls',
				'data-name'=>'Role[permissions][]',
				'data-label'=>Module::t('Assigned')
			]) ?>
		</div>
		<div class="col-md-1">
			<div id="select-permission-controls">

			</div>
		</div>
		<div class="col-md-3">
			<div id="select-permissions">

			</div>
		</div>
	</div>




	<?php ActiveForm::end(); ?>
</div>