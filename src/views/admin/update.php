<?php

use yii\helpers\Html;
use stephencozart\yii2\modules\user\Module;

/* @var $this yii\web\View */
/* @var $model stephencozart\yii2\modules\user\models\User */

$this->title = Module::t('Update User {email}', ['email'=>$model->email]);
$this->params['breadcrumbs'][] = ['label' => Module::t('Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->email, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Module::t('Update');
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
