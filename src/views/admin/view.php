<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use stephencozart\yii2\modules\user\Module;

/* @var $this yii\web\View */
/* @var $model stephencozart\yii2\modules\user\models\User */
/* @var $showUpdateButton bool */
/* @var $showDeleteButton bool */

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => Module::t('Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1>
	    <?= Html::encode($this->title) ?>
	    <?php if ($showUpdateButton): ?>
		    <?= Html::a(Module::t('Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary pull-right']) ?>
	    <?php endif; ?>
	    <?php if ($showDeleteButton): ?>
		    <?= Html::a(Module::t('Delete'), ['delete', 'id' => $model->id], [
			    'class' => 'btn btn-danger pull-right',
			    'data' => [
				    'confirm' => Module::t('Are you sure you want to delete this item?'),
				    'method' => 'post',
			    ],
		    ]) ?>
	    <?php endif; ?>
    </h1>

    <p>


    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'last_name',
            'email:email',
            'username',
            [
	            'attribute'=>'activation_code',
	            'value'=>''
            ],
            [
	            'attribute'=>'status_id',
	            'value'=>$model->getStatusName()
            ],
            'created_on',
            'updated_on',
        ],
    ]) ?>

</div>
