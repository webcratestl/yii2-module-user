<?php

use yii\helpers\Html;
use stephencozart\yii2\modules\user\Module;

/* @var $this yii\web\View */
/* @var $model stephencozart\yii2\modules\user\models\User */

$this->title = Module::t('Create User');
$this->params['breadcrumbs'][] = [
	'label' => Module::t('Users'), 'url' => ['index']
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <h1><?= Html::encode($this->title) ?></h1>
	<hr>
	<?= $this->render('_form', [
		'model' => $model,
	]) ?>



</div>
