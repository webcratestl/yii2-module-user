<?php

use yii\helpers\Html;
use yii\grid\GridView;
use stephencozart\yii2\modules\user\Module;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */
/* @var $showCreateButton bool */

$this->title = Module::t('Roles');
$this->params['breadcrumbs'][] = ['label' => Module::t('Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
if (empty($actionColumnTemplate)) {
	$actionColumnTemplate = '';
}
?>
<div class="user-index">

	<h1>
		<?= Html::encode($this->title) ?>
		<?php if ($showCreateButton): ?>
			<?= Html::a(Module::t('Create Role'), '#', [
				'class' => 'btn btn-success pull-right',
				'data-toggle'=>'modal',
				'data-target'=>'#user-module-modal',
				'data-url'=>\yii\helpers\Url::to(['create-role']),
				'data-title'=>Module::t('Create Role')
			]) ?>
		<?php endif; ?>
	</h1>

	<p>

	</p>

	<?= GridView::widget([
		'id'=>'roles-grid-view',
		'dataProvider' => $dataProvider,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'name',
			'description',

			[
				'class' => 'yii\grid\ActionColumn',
				'template'=> $actionColumnTemplate,
				'buttons'=>[
					'update'=>function ($url, $model, $key) {
						return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
							'data-url'=>\yii\helpers\Url::to(['update-role','id'=>$model->name]),
							'data-toggle'=>'modal',
							'data-target'=>'#user-module-modal',
							'data-title'=>Module::t('Update Role')
						]);
					}
				]
			],
		],
	]); ?>


</div>
<?php echo $this->render('_modal'); ?>
