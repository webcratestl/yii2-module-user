<?php

use yii\helpers\Html;
use yii\grid\GridView;
use stephencozart\yii2\modules\user\Module;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $showCreateButton bool */
/* @var $actionColumnTemplate string */

$this->title = Module::t('Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1>
	    <?= Html::encode($this->title) ?>
	    <?php if ($showCreateButton): ?>
		    <?= Html::a(Module::t('Roles'), ['roles'], ['class' => 'btn btn-info pull-right']) ?>
	        <?= Html::a(Module::t('Create User'), ['create'], ['class' => 'btn btn-success pull-right']) ?>
	    <?php endif; ?>
    </h1>

    <p>

    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'first_name',
            'last_name',
            'email:email',
            'username',
            // 'password',
            // 'activation_code',
            // 'reset_code',
            // 'auth_key',
            // 'status_id',
            'created_on',
            'updated_on',

            [
	            'class' => 'yii\grid\ActionColumn',
	            'template'=>$actionColumnTemplate
            ],
        ],
    ]); ?>

</div>
