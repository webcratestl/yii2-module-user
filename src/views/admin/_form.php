<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use stephencozart\yii2\modules\user\Module;

/* @var $this yii\web\View */
/* @var $model stephencozart\yii2\modules\user\models\User */
/* @var $form yii\widgets\ActiveForm */

$availableRoles = \yii\helpers\ArrayHelper::map(Module::getSystemRoles(), function(\yii\rbac\Item $item) {
	return $item->name;
},
function(\yii\rbac\Item $item) {
	$name = $item->description ? $item->name : $item->name;
	return Module::t($name);
});
?>

<div class="user-form">

    <?php $form = ActiveForm::begin([
	    'layout'=>'horizontal',
	    'fieldConfig' => [
		    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
		    'horizontalCssClasses' => [
			    'label' => 'col-sm-4',
			    'offset' => 'col-sm-offset-4',
			    'wrapper' => 'col-sm-8',
			    'error' => '',
			    'hint' => '',
		    ],
	    ],
    ]); ?>

	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?= Module::t('User Information') ?>
				</div>
				<div class="panel-body">
					<?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

					<?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

					<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

					<?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

					<?php if ($model->isNewRecord): ?>
						<?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
					<?php endif; ?>

					<?= $form->field($model, 'status_id')->dropDownList(\stephencozart\yii2\modules\user\models\User::getStatusList()) ?>
				</div>
			</div>

		</div>
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?= Module::t('Roles') ?>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-5">
							<div id="roles">
								<?php if (Yii::$app->authManager): ?>
									<label for="availableRoles"><?= Module::t('Available') ?></label>
									<?= Html::dropDownList('availableRoles', $model->roleNames, $availableRoles, [
										'multiple'=>'multiple',
										'class'=>'form-control',
										'data-toggle'=>'multiselect',
										'data-target'=>'#select-roles',
										'data-controls'=>'#select-controls',
										'data-name'=>'roles[]',
										'data-label'=>Module::t('Assigned')
									]) ?>
								<?php else: ?>
									<p><?= Module::t('Auth Manager is not enabled.') ?></p>
								<?php endif; ?>
							</div>
						</div>
						<div class="col-md-2">
							<div id="select-controls">

							</div>
						</div>
						<div class="col-md-5">
							<div id="select-roles">

							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="row button-row">
		<div class="col-sm-12 text-right">
			<hr>
			<?= Html::a(Module::t('&larr; Cancel'), ['index'], ['class'=>'btn btn-link']) ?>
			<?= Html::submitButton(Module::t($model->isNewRecord ? 'Create User' : 'Update User'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		</div>
	</div>

    <?php ActiveForm::end(); ?>

</div>

