<?php
/* @var $this yii\web\View */
?>

<div class="modal fade" id="user-module-modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<?php \yii\widgets\Pjax::begin([
					'options'=>['id'=>'modal-content-target'],
					'enablePushState'=>false
				]) ?>

				<?php \yii\widgets\Pjax::end() ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" data-type="submit">Save changes</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
