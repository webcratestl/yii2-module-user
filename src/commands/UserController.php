<?php
/**
 * Created by JetBrains PhpStorm.
 * User: stephencozart
 * Date: 7/25/15
 * Time: 4:33 PM
 * To change this template use File | Settings | File Templates.
 */

namespace stephencozart\yii2\modules\user\commands;

use stephencozart\yii2\modules\user\models\User;
use stephencozart\yii2\modules\user\Module;
use Exception;
use yii\base\InvalidConfigException;
use yii\console\Controller;
use Yii;

/**
 * This command provides some console utilities
 */
class UserController extends Controller {

	public function init()
	{
		parent::init();
		Module::initLocale();
	}

	/**
	 * Creates a user account with an email and password
	 * @param string $email
	 * @param string $password
	 * @return int
	 */
	public function actionCreate($email, $password)
	{

		$User = new User(['scenario'=>User::SCENARIO_CREATE_CONSOLE]);
		$User->email = $email;
		$User->password = $password;
		$User->username = $email;
		$User->status_id = User::STATUS_ACTIVE;

		if ($User->validate()) {
			$User->password = Yii::$app->security->generatePasswordHash($password);
			if ($User->save(false)) {
				return 0;
			}
		}
		var_dump($User->getErrors());
		return 1;
	}

	/**
	 * Installs the permissions the module needs to operate
	 *
	 * @return int
	 */
	public function actionInstallPermissions()
	{
		$permissions = [
			'create-user'=>'Create new users',
			'update-user'=>'Update existing users',
			'view-user'=>'View user account information',
			'delete-user'=>'Delete users',
			'create-roles'=>'Create roles',
			'update-roles'=>'Update roles',
			'delete-roles'=>'Delete roles'
		];

		try {
			$authManager = $this->getAuthManager();
			foreach($permissions as $permission => $description) {
				if ($perm = $authManager->getPermission($permission)) {
					continue;
				}
				$perm = $authManager->createPermission($permission);
				$perm->description = $description;

				$authManager->add($perm);
			}
		} catch(\Exception $e) {
			echo $e->getMessage()."\n";
			return 1;
		}
		echo Module::t('Permissions installed successfully!');
		return 0;

	}

	public function actionCreateRole($roleName, $description = null)
	{
		try {

			$authManger = $this->getAuthManager();

			if ($role = $authManger->getRole($roleName)) {
				throw new Exception(Module::t('Role "{role}" already exists', ['role'=>$roleName]));
			}

			$role = $authManger->createRole($roleName);
			$role->description = $description;

			if ($authManger->add($role)) {
				echo Module::t('Role "{role}" has been created', ['role'=>$roleName])."\n";
				return 0;
			} else {
				throw new Exception(Module::t('An unknown error has occurred trying to create the role "{role}"', ['role'=>$roleName]));
			}


		} catch(\Exception $e) {
			echo $e->getMessage()."\n";
			return 1;
		}
	}

	public function actionCreatePermission($permissionName, $description)
	{
		try {
			$authManager = $this->getAuthManager();
			if ($perm = $authManager->getPermission($permissionName)) {
				throw new Exception(Module::t('Permission "{permission}" already exists', ['permission'=>$permissionName]));
			}
			$perm = $authManager->createPermission($permissionName);
			$perm->description = $description;

			if ($authManager->add($perm)) {
				echo Module::t('Permission "{permission}" has been created', ['permission'=>$permissionName])."\n";
				return 0;
			} else {
				throw new Exception(Module::t('An unknown error has occurred trying to create the permission "{permission}"', ['permission'=>$permissionName]));
			}

		} catch (Exception $e) {
			echo $e->getMessage()."\n";
			return 1;
		}
	}

	public function actionAssignRole($email, $roleName)
	{
		try {
			$authManager = $this->getAuthManager();

			/** @var User $User */
			$User = User::findByEmail($email);
			if ($User === null) {
				throw new Exception(Module::t('User not found using email "{email}"', ['email'=>$email]));
			}

			$role = $authManager->getRole($roleName);
			if ($role === null) {
				throw new Exception(Module::t('Role not found using role name "{role}"', ['role'=>$roleName]));
			}

			if ($authManager->assign($role, $User->getPrimaryKey())) {
				echo Module::t('Role "{role}" has been assigned to "{email}"', [
						'role'=>$roleName,
						'email'=>$email
					])."\n";
				return 0;
			} else {
				throw new Exception(Module::t('An unknown error has occurred trying to create the permission "{permission}"', ['permission'=>$permissionName]));
			}

		} catch (Exception $e) {
			echo $e->getMessage()."\n";
			return 1;
		}
	}

	public function actionAssignRolePermission($roleName, $permissionName)
	{
		try {
			$authManager = $this->getAuthManager();

			$role = $authManager->getRole($roleName);
			$permission = $authManager->getPermission($permissionName);

			if ($role === null) {
				throw new Exception(Module::t('Role not found using role name "{role}"', ['role'=>$roleName]));
			}

			if ($permission === null) {
				throw new Exception(Module::t('Permission not found using permission name "{permission}"', ['permission'=>$permissionName]));
			}

			if ($authManager->addChild($role, $permission)) {
				echo Module::t('"{permission}" has been assigned to the "{role}"', ['permission'=>$permissionName, 'role'=>$roleName])."\n";
				return 0;
			} else {
				throw new Exception(Module::t('An unknown error has occurred trying to create the permission "{permission}"', ['permission'=>$permissionName]));
			}

		} catch (Exception $e) {
			echo $e->getMessage()."\n";
			return 1;
		}
	}

	protected function getAuthManager()
	{
		if ($authManager = Yii::$app->authManager) {
			return $authManager;
		}
		throw new InvalidConfigException(Module::t('Auth Manager is not enabled.'));
	}



}