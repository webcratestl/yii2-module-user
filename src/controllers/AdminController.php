<?php

namespace stephencozart\yii2\modules\user\controllers;

use stephencozart\yii2\modules\user\models\Role;
use Yii;
use stephencozart\yii2\modules\user\models\User;
use yii\base\Event;
use yii\base\Exception;
use yii\bootstrap\Alert;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\rbac\Item;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\View;
use stephencozart\yii2\modules\user\Module;

/**
 * AdminController implements the CRUD actions for User model.
 */
class AdminController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
	        'access' => [
		        'class' => AccessControl::className(),
		        'rules' => [
			        [
				        'allow' => true,
				        'actions' => ['create'],
				        'roles' => ['create-user'],
			        ],
			        [
				        'allow' => true,
				        'actions' => ['update'],
				        'roles' => ['update-user'],
			        ],
			        [
				        'allow'=> true,
				        'actions' => ['view'],
				        'roles' => ['view-user']
			        ],
			        [
				        'allow'=> true,
				        'actions' => ['delete'],
				        'roles' => ['delete-user']
			        ],
			        [
				        'allow'=> true,
				        'actions' => ['index'],
				        'roles' => ['create-user','update-user','view-user','delete-user']
			        ],
			        [
				        'allow'=>true,
				        'actions'=>['roles'],
				        'roles'=>['create-roles','update-roles']
			        ],
			        [
				        'allow'=>true,
				        'actions'=>['create-role'],
				        'roles'=>['create-roles']
			        ],
			        [
				        'allow'=>true,
				        'actions'=>['update-role'],
				        'roles'=>['update-roles']
			        ],
			        [
				        'allow'=>false,
			        ]
		        ]
		    ]
        ];
    }

	public function init()
	{
		parent::init();
		$force = defined('YII_ENV') === false || YII_ENV === 'dev';
		list($path, $url) = Yii::$app->assetManager->publish(__DIR__ .'/../assets',['forceCopy'=>$force]);

		Yii::$app->view->registerJsFile($url.'/sc.multi.select.js', [
			'depends'=>[
				'yii\bootstrap\BootstrapAsset',
				'yii\bootstrap\BootstrapPluginAsset'
			],
			'position'=>View::POS_END
		]);

		Yii::$app->view->registerJsFile($url.'/user-module.js', [
			'depends'=>[
				'yii\bootstrap\BootstrapAsset',
				'yii\bootstrap\BootstrapPluginAsset'
			],
			'position'=>View::POS_END
		]);
	}

	public function actionRoles()
	{
		$roles = [];
		$buttons = [];

		if (Yii::$app->user->can('update-roles')) {
			$buttons[] = '{update}';
		}

		if (Yii::$app->user->can('delete-roles')) {
			$buttons[] = '{delete}';
		}

		if ($authManager = Yii::$app->authManager) {
			$roles = $authManager->getRoles();
		}
		$dataProvider = new ArrayDataProvider([
			'allModels'=>$roles,
			'key'=>function(Item $item) {
				return $item->name;
			}
		]);

		return $this->render('roles', [
			'dataProvider'=>$dataProvider,
			'actionColumnTemplate'=>implode(' ', $buttons),
			'showCreateButton'=>Yii::$app->user->can('create-roles')
		]);
	}

	public function actionUpdateRole($id)
	{
		$model = new Role();

		if ($authManager = Yii::$app->authManager) {
			$role = $authManager->getRole($id);
			if ($role) {
				$model->name = $role->name;
				$model->description = $role->description;
				foreach($authManager->getPermissionsByRole($id) as $perm) {
					$model->permissions[] = $perm->name;
				}
			}

			$permissions = $authManager->getPermissions();
			$permissionsListData = ArrayHelper::map($permissions, function(Item $item) {
					return $item->name;
				},
				function(Item $item) {
					return Module::t($item->name);
				});

		} else {
			throw new Exception(Module::t('Auth manager is not enabled'));
		}

		if ($model->load(Yii::$app->request->post())) {
			if ($model->save()) {
				return Alert::widget([
					'options'=>[
						'class'=>'alert-success',
					],
					'body'=>Module::t('{role} Has been updated', ['role'=>$model->name])
				]);
			}
		}


		return $this->renderPartial('_role_form', [
			'model'=>$model,
			'permissions'=>$permissions,
			'permissionsListData'=>$permissionsListData
		]);
	}

	public function actionCreateRole()
	{
		$model = new Role(['scenario'=>'insert']);
		if ($authManager = Yii::$app->authManager) {
			$permissions = $authManager->getPermissions();
			$permissionsListData = ArrayHelper::map($permissions, function(Item $item) {
				return $item->name;
			},
			function(Item $item) {
				return Module::t($item->name);
			});

		} else {
			throw new Exception(Module::t('Auth manager is not enabled'));
		}
		if ($model->load(Yii::$app->request->post())) {

			if ($model->save()) {
				return Alert::widget([
					'options'=>[
						'class'=>'alert-success',
					],
					'body'=>Module::t('{role} Has been created', ['role'=>$model->name])
				]);
			}
		}

		return $this->renderPartial('_role_form', [
			'model'=>$model,
			'permissions'=>$permissions,
			'permissionsListData'=>$permissionsListData
		]);
	}

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find(),
        ]);
	    $buttons = [];
	    $checks = [
		    'view-user'=>'{view}',
		    'update-user'=>'{update}',
		    'delete-user'=>'{delete}'
	    ];
	    foreach($checks as $permission=>$button) {
		    if (Yii::$app->user->can($permission)) {
			    $buttons[] = $button;
		    }
	    }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
	        'showCreateButton'=>Yii::$app->user->can('create-user'),
	        'actionColumnTemplate'=>implode(' ', $buttons)
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
	        'showDeleteButton'=>Yii::$app->user->can('delete-user'),
	        'showUpdateButton'=>Yii::$app->user->can('update-user')
        ]);
    }

	public function syncRoles(Event $event)
	{
		/** @var User $model */
		$model = $event->sender;

		if ($authManager = Yii::$app->authManager) {
			$transaction = Yii::$app->db->beginTransaction();
			try {
				$authManager->revokeAll($model->id);
				foreach($model->roleNames as $roleName) {
					if ($role = $authManager->getRole($roleName)) {
						$authManager->assign($role, $model->id);
					}
				}
				$transaction->commit();
			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			}

		}

	}

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
	    $model->on(ActiveRecord::EVENT_AFTER_INSERT, array($this, 'syncRoles'));

        if ($model->load(Yii::$app->request->post())) {
	        $model->roleNames = Yii::$app->request->post('roles',[]);
	        if ($model->save()) {
		        Yii::$app->session->setFlash('success', Module::t('{user} has been created',['user'=>$model->email]));
		        return $this->redirect(['view', 'id' => $model->id]);
	        }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
	    $model->on(ActiveRecord::EVENT_AFTER_UPDATE, array($this, 'syncRoles'));

        if ($model->load(Yii::$app->request->post())) {
	        $model->roleNames = Yii::$app->request->post('roles',[]);
	        if ($model->save()) {
		        Yii::$app->session->setFlash('success', Module::t('{user} has been updated',['user'=>$model->email]));
		        return $this->redirect(['view', 'id' => $model->id]);
	        }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	    $model = $this->findModel($id);

	    $model->on(ActiveRecord::EVENT_AFTER_DELETE, function(Event $event) {
		    /** @var User $model */
		    $model = $event->sender;

		    if ($authManager = Yii::$app->authManager) {
			    $authManager->revokeAll($model->id);
		    }
	    });

        if ($model->delete()) {
	        Yii::$app->session->setFlash('success', Module::t('{user} has been deleted',['user'=>$model->email]));
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
