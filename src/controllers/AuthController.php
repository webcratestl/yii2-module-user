<?php
/**
 * Created by JetBrains PhpStorm.
 * User: stephencozart
 * Date: 7/26/15
 * Time: 8:24 PM
 * To change this template use File | Settings | File Templates.
 */

namespace stephencozart\yii2\modules\user\controllers;


use stephencozart\yii2\modules\user\models\LoginForm;
use stephencozart\yii2\modules\user\models\User;
use stephencozart\yii2\modules\user\Module;
use yii\helpers\Url;
use yii\base\Event;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;
use yii\web\HttpException;

class AuthController extends Controller {

	public function behaviors() {

		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow'=>true,
						'roles'=>['?'],
						'actions'=>['forgot-password-complete','login', 'forgot-password','register']
					],
					[
						'allow'=>true,
						'roles'=>['@'],
						'actions'=>['logout']
					],
					[
						'allow'=>false
					]
				]
			]
		];


	}

	public function actionForgotPassword()
	{
		$model = new User();

		if ($model->load(Yii::$app->request->post())) {

			/** @var User $user */
			$user = User::findByEmail($model->email);

			if ($user) {
				$user->reset_code = Module::generateRandomString();
				if ($user->save()) {
					Yii::$app->trigger('sc.user.forgot-password', new Event(['sender'=>$user]));
					return $this->refresh();
				}
			} else {
				$model->addError('email', Module::t('We could not locate them email address "{email}" in our system.',['email'=>$model->email]));
			}

		}

		return $this->render('forgot-password', [
			'model'=>$model
		]);
	}

	public function actionForgotPasswordComplete($code)
	{
		if (Yii::$app->user->isGuest === false) {
			return $this->goBack();
		}

		/** @var User $User */
		$User = User::find()->where(['reset_code'=>$code])->one();

		if ($User === null) {
			throw new HttpException(400, Module::t('Invalid reset code'));
		}

		$model = new User(['scenario'=>User::SCENARIO_ACTIVATE]);

		if ($model->load(Yii::$app->request->post())) {
			if ($model->validate()) {
				$User->password = Yii::$app->security->generatePasswordHash($model->password);
				$User->reset_code = null;
				$User->updated_on = date('Y-m-d H:i:s');
				$User->save(false);
				Yii::$app->trigger('sc.user.forgot-password-complete', new Event(['sender'=>$User]));
				if ($this->login($User->username, $model->password)) {
					return $this->goHome();
				} else {
					return $this->redirect(['login']);
				}
			}
		}

		return $this->render('forgot-password-complete', array(
			'model'=>$model
		));
	}

	public function actionLogin()
	{
		if (Yii::$app->user->isGuest === false) {
			return $this->goHome();
		}

		$model = new LoginForm();

		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->goBack();
		} else {
			return $this->render('login', [
				'model' => $model,
			]);
		}
	}

	public function actionLogout()
	{
		Yii::$app->user->logout();
		return $this->goHome();
	}

	public function actionRegister()
	{
		$model = new User(['scenario'=>User::SCENARIO_REGISTER]);

		if ($model->load(Yii::$app->request->post())) {
			$model->activation_code = Module::generateRandomString();
			$model->status_id = User::STATUS_ACTIVE;

			if ($model->validate()) {
				$plain = $model->password;
				$model->password = Yii::$app->security->generatePasswordHash($plain);
				if ($model->save(false)) {
					Yii::$app->trigger('sc.user.register', new Event(['sender'=>$model]));
					if ($this->login($model->username, $plain)) {
						return $this->goHome();
					}
				}
			}
		}

		return $this->render('register', [
			'model'=>$model
		]);
	}

	protected function login($username, $password)
	{
		if (!Yii::$app->user->isGuest) {
			return $this->goBack();
		}

		$model = new LoginForm();
		$model->username = $username;
		$model->password = $password;

		return $model->login();
	}



}
